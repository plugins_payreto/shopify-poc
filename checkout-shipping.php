 <?php 
 	session_start();
  	include('con_db/con_db.php');
	$customerinfo = "select * from tbl_shop_customer where cart_token='".$_COOKIE['cart_token']."'";
	$customerinfo = $db->query($customerinfo);
	$customerinfo = $customerinfo->fetch_object();
 ?>
 <style>
	 table{
	 	 border-collapse: collapse;
	 	 border: 1px solid #dee2e6;
	 }
 </style>
 <div class="col-md-12">
   <div class="row">
	   	<div class="table-responsive">
	   		<table class="table">
			  <tbody>
			    <tr>
			      <td class="col-md-1" style="color:#999797">Contact</td>
			      <td class="col-md-8"><?php echo (isset($customerinfo->customer_contact)) ? $customerinfo->customer_contact:'' ?></td>
			    </tr>
			    <tr>
			      <td style="color:#999797">Ship to</td>
			      <td><?php echo (isset($customerinfo->customer_address)) ? $customerinfo->customer_address:'' ?></td>
			    </tr>
			  </tbody>
			</table>
		</div>
		<h4>Shipping method</h4>
		<div class="table-responsive" id="shiprates">
	   		<div class="loader"></div>
		</div>
   </div>
</div>
<div class="col-md-12 pl-0">
	<span class="float-left" onclick="callAjax('checkout-customerinfo.php')" style="color:#007bff;cursor: pointer;">Return to customer information</span>
	<button class="btn btn-info float-right" onclick="nextStep('checkout-payment.php')">Continue to payment method</button>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
		  url: "shipping-ajax.php?shop=<?php echo $shop ?>",
		  method: "GET"
		}).done(function(data){
			$("#shiprates").empty();
			$("#shiprates").append(data);
		});
	});
</script>
