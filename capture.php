<?php 
	include('con_db/con_db.php');
	$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
	$data = file_get_contents('php://input');
	$verified = verify_webhook($data, $hmac_header);

	if($verified){
		$decodedData = json_decode($data);
		if($decodedData->kind == "capture"){
			error_log("transaction shopify response ".$data."\n", 3, "/var/www/html/shopify-poc/my-errors.log");
		}
	}
?>