-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: shopifypoc
-- ------------------------------------------------------
-- Server version	5.5.56-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `refund_webhook`
--

DROP TABLE IF EXISTS `refund_webhook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refund_webhook` (
  `id_json` int(11) NOT NULL AUTO_INCREMENT,
  `json_body` text,
  `json_body_sent` text,
  PRIMARY KEY (`id_json`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refund_webhook`
--

LOCK TABLES `refund_webhook` WRITE;
/*!40000 ALTER TABLE `refund_webhook` DISABLE KEYS */;
INSERT INTO `refund_webhook` VALUES (1,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"},{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"},{\"name\":\"amount\",\"value\":null,\"message\":\"may not be empty\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:24:40+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_b4c5f92279bd4c59888260bc78a918c9\"}',NULL),(2,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"},{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:28:28+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_3d3faf65d17742189915cf2b4fd10254\"}',NULL),(3,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"},{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:32:56+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_9adeed1fff66495ebdc68b6310f032d3\"}',NULL),(4,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"},{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:33:32+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_3c547f0bb30c4228882afb358dd4edaa\"}',NULL),(5,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"},{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:50:36+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_be0423e51aad4b6687fb2790f156c574\"}',NULL),(6,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"},{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:51:22+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_c4ce0e31609b4104b56f9c7a881efe17\"}',NULL),(7,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"},{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:51:27+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_42383c358f2147779f64ffcd06b29516\"}',NULL),(8,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"},{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:51:44+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_71f7ac02d4364f75b4fa5b9b89936c24\"}',NULL),(9,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"},{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:52:07+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_9c40e24bc9bd4444b7cca86c7bee7f90\"}',NULL),(10,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"},{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:52:07+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_cf091bdf783a4f81890c8a9362629d5b\"}',NULL),(11,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"},{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:52:29+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_96f3c8e89e0a4b0a83474dfeb3e44ad1\"}',NULL),(12,'{\"result\":{\"code\":\"200.300.404\",\"description\":\"invalid or missing parameter\",\"parameterErrors\":[{\"name\":\"paymentType\",\"value\":\"RF\",\"message\":\"must be one of [PA, DB, CD, PA.CP]\"},{\"name\":\"paymentBrand\",\"value\":null,\"message\":\"card properties must be set\"}]},\"buildNumber\":\"f7c953080e1b6da8cfcf6cb8ca1a460db91503a6@2018-09-11 12:23:05 +0000\",\"timestamp\":\"2018-09-12 11:53:12+0000\",\"ndc\":\"8a8294174d0a8edd014d0abf32d201ac_eb2472063d354b8183e65887cc0352a3\"}',NULL);
/*!40000 ALTER TABLE `refund_webhook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_appsettings`
--

DROP TABLE IF EXISTS `tbl_appsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_appsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key` varchar(300) DEFAULT NULL,
  `redirect_url` varchar(300) DEFAULT NULL,
  `permissions` text,
  `shared_secret` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_appsettings`
--

LOCK TABLES `tbl_appsettings` WRITE;
/*!40000 ALTER TABLE `tbl_appsettings` DISABLE KEYS */;
INSERT INTO `tbl_appsettings` VALUES (1,'c4f2cac38506ef88a829daf4b71d02ec','https://31fe3c33.ngrok.io/shopify-poc/save_shop.php','read_orders,write_orders,write_checkouts,read_shipping,write_shipping','293f36acc258d9c590d1b23efd6d2910');
/*!40000 ALTER TABLE `tbl_appsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_card_brands`
--

DROP TABLE IF EXISTS `tbl_card_brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_card_brands` (
  `id_card` int(11) NOT NULL AUTO_INCREMENT,
  `card_brand` varchar(10) DEFAULT NULL,
  `card_title` varchar(10) DEFAULT NULL,
  `card_image` text,
  PRIMARY KEY (`id_card`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_card_brands`
--

LOCK TABLES `tbl_card_brands` WRITE;
/*!40000 ALTER TABLE `tbl_card_brands` DISABLE KEYS */;
INSERT INTO `tbl_card_brands` VALUES (1,'VISA','visa','images/visa.png'),(2,'MASTER','master','images/master.png'),(3,'JCB','jcb','images/jcb.png'),(4,'DINERS','diners','images/diners.png');
/*!40000 ALTER TABLE `tbl_card_brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_shop_customer`
--

DROP TABLE IF EXISTS `tbl_shop_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_shop_customer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `customer_contact` varchar(100) DEFAULT NULL,
  `customer_firstname` varchar(50) DEFAULT NULL,
  `customer_lastname` varchar(50) DEFAULT NULL,
  `customer_address` text,
  `customer_city` varchar(50) DEFAULT NULL,
  `customer_country` varchar(50) DEFAULT NULL,
  `customer_province` varchar(50) DEFAULT NULL,
  `customer_postal_code` varchar(6) DEFAULT NULL,
  `shop` varchar(100) DEFAULT NULL,
  `cart_token` text,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_shop_customer`
--

LOCK TABLES `tbl_shop_customer` WRITE;
/*!40000 ALTER TABLE `tbl_shop_customer` DISABLE KEYS */;
INSERT INTO `tbl_shop_customer` VALUES (1,'yoseph@esphere.id','Yoseph','Fernando','Bl. Al, RT.5/RW.14, Duri Kepa, Kb. Jeruk, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11510','jakarta','128789381189','1408611909701','11510','yosephtest.myshopify.com','86f9f53c0b32cd3a976363c3a9674228'),(2,'yoseph@esphere.id','Yoseph','Fernando','FC Bayern MÃ¼nchen AG SÃ¤bener StraÃŸe 51-57 D-81547 MÃ¼nchen','MÃ¼nchen','138802135110','','HRB 14','yosephtesteu.myshopify.com','d8e8b84a4f9e08e134655ef36f661ee1'),(3,'yoseph@esphere.id','Yoseph','Fernando','FC Bayern MÃƒÂ¼nchen AG SÃƒÂ¤bener StraÃƒÅ¸e 51-57 D-81547 MÃƒÂ¼nchen   ','MÃ¼nchen','138802135110','','HRB 14','yosephtesteu.myshopify.com','644a1580fecfd044a380acba9f2b9183'),(4,'yoseph@esphere.id','Yoseph','Fernando','FC Bayern MÃƒÂ¼nchen AG SÃƒÂ¤bener StraÃƒÅ¸e 51-57 D-81547 MÃƒÂ¼nchen','MÃ¼nchen','138802135110','','HRB 14','yosephtesteu.myshopify.com','c91d1216c1682113f243e2d8ec67b771'),(5,'yoseph@esphere.id','Yoseph','Fernando','FC Bayern MÃƒÂ¼nchen AG SÃƒÂ¤bener StraÃƒÅ¸e 51-57 D-81547 MÃƒÂ¼nchen  ','MÃ¼nchen','138802135110','','HRB 14','yosephtesteu.myshopify.com','bc3d5d98e82a2c3bf93ca5bf17c6c845'),(6,'yoseph@esphere.id','Yoseph','Fernando','FC Bayern MÃƒÂ¼nchen AG SÃƒÂ¤bener StraÃƒÅ¸e 51-57 D-81547 MÃƒÂ¼nchen ','MÃ¼nchen','138802135110','','HRB 14','yosephtesteu.myshopify.com','620ace7ce92b617991f4476f0389d2ae'),(7,'fernandoyoseph6@gmail.com','Yoseph','Fernando','FC Bayern MÃƒÂ¼nchen AG SÃƒÂ¤bener StraÃƒÅ¸e 51-57 D-81547 MÃƒÂ¼nchen ','MÃ¼nchen','138802135110','','HRB 14','yosephtesteu.myshopify.com','7df99fb014654fcdbdc70f54fbc9f852'),(8,'yoseph@esphere.id','Yoseph','Fernando','FC Bayern MÃƒÂ¼nchen AG SÃƒÂ¤bener StraÃƒÅ¸e 51-57 D-81547 MÃƒÂ¼nchen','MÃ¼nchen','138802135110','','HRB 14','yosephtesteu.myshopify.com','aef9335eebf8522ea4cbab42ffe4c385'),(9,'yoseph@esphere.id','Yoseph','Fernando','FC Bayern MÃƒÂ¼nchen AG SÃƒÂ¤bener StraÃƒÅ¸e 51-57 D-81547 MÃƒÂ¼nchen ','MÃ¼nchen','138802135110','','HRB 14','yosephtesteu.myshopify.com','ce5c79f2ed700a6983ec450e57f682ac');
/*!40000 ALTER TABLE `tbl_shop_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_store_conf`
--

DROP TABLE IF EXISTS `tbl_store_conf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_store_conf` (
  `id_conf` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(100) DEFAULT NULL,
  `user_id` text,
  `password` text,
  `card_collection` text,
  `entity_id` text,
  `trans_mod` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_conf`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_store_conf`
--

LOCK TABLES `tbl_store_conf` WRITE;
/*!40000 ALTER TABLE `tbl_store_conf` DISABLE KEYS */;
INSERT INTO `tbl_store_conf` VALUES (1,'yosephtest.myshopify.com','8a8294174d0a8edd014d0a9f9743007b','jGNhYaX5','VISA,MASTER,JCB','8a8294174d0a8edd014d0abf32d201ac','DB'),(2,'yosephtesteu.myshopify.com','8a8294174d0a8edd014d0a9f9743007b','jGNhYaX5','VISA,MASTER,JCB','8a8294174d0a8edd014d0abf32d201ac','DB');
/*!40000 ALTER TABLE `tbl_store_conf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_transaction`
--

DROP TABLE IF EXISTS `tbl_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_transaction` (
  `id_trans` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` text,
  `reference_id` text,
  PRIMARY KEY (`id_trans`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_transaction`
--

LOCK TABLES `tbl_transaction` WRITE;
/*!40000 ALTER TABLE `tbl_transaction` DISABLE KEYS */;
INSERT INTO `tbl_transaction` VALUES (1,'594433474630','8a82944965c8ea970165cd53dacf6875'),(2,'594445533254','8a82944a65c909650165cd6a2bfa6c91'),(3,'594448547910','8a82944965c8ea970165cd706ae73853'),(4,'594450022470','8a82944965c8ea970165cd735ccd47c8'),(5,'594454675526','8a82944965c8ea970165cd7c9e2772ac'),(6,'594457952326','8a82944a65c909650165cd82ccce53c9'),(7,'594459066438','8a82944a65c909650165cd84d6ae5c2f'),(8,'594478137414','8a82944a65c909650165cda4ad514f5b'),(9,'595187826758','8a82944965c8ea970165d0bdae8f2e76'),(10,'595200901190','8a82944a65c909650165d0cfcaad06c2'),(11,'595204505670','8a82944965c8ea970165d0d5604f70fa'),(12,'595206012998','8a82944965c8ea970165d0d81d747978'),(13,'595211714630','8a82944965c8ea970165d0e21ac91499'),(14,'595214041158','8a82944965c8ea970165d0e71feb2182'),(15,'595217678406','8a82944a65c909650165d0ee7b895cf6'),(16,'595220004934','8a82944965c8ea970165d0f290ed42f7'),(17,'595221446726','8a82944a65c909650165d0f5ab9673e1'),(18,'595222364230','8a82944a65c909650165d0f75a3e77b6'),(19,'595231932486','8a82944a65c909650165d10a9d901f6d'),(20,'595247398982','8a82944965c8ea970165d12e85c73c44'),(21,'595254607942','8a82944965c8ea970165d13c01cf4856'),(22,'595281018950','8a82944965c8ea970165d17f6c4019a7'),(23,'595292684358','8a82944a65c909650165d19ef1107e47'),(24,'595298385990','8a82944965c8ea970165d1b013696908'),(25,'595335381062','8a82944a65c909650165d1f7fd48420f');
/*!40000 ALTER TABLE `tbl_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_usersettings`
--

DROP TABLE IF EXISTS `tbl_usersettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usersettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token` text NOT NULL,
  `store_name` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_usersettings`
--

LOCK TABLES `tbl_usersettings` WRITE;
/*!40000 ALTER TABLE `tbl_usersettings` DISABLE KEYS */;
INSERT INTO `tbl_usersettings` VALUES (1,'b7bc6e6e220f574f7a085813d855f94d','yosephtest.myshopify.com'),(5,'79e8a98beb911c0d8098348feb02bd7d','yosephtesteu.myshopify.com');
/*!40000 ALTER TABLE `tbl_usersettings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-13 15:23:50
