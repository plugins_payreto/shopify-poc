<?php 
	session_start();

	require 'vendor/autoload.php';
  	use sandeepshetty\shopify_api;
  	include('con_db/con_db.php');

  	$cardbrands = "select card_brand, card_title from tbl_card_brands";
	$cardbrands = $db->query($cardbrands);
?>
<!DOCTYPE html>
<html>
	<head>
		 <?php include('includes/header.php'); ?>
	</head>
	<body>
		<div class="container mt-5">
			<div class="row">
				<form class="form-group col-md-12" method="post" action="save_conf.php?shop=<?php echo $shop ?>">
					<div class="col-md-12">
						<?php 
							if(isset($_SESSION['message_success'])){
						?>
							<div class="alert alert-success alert-dismissible fade show" role="alert">
							  <strong><?php echo $_SESSION['message_success']?></strong>
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
							</div>
						<?php }else if(isset($_SESSION['message_error'])){ ?>
							<div class="alert alert-danger alert-dismissible fade show" role="alert">
							  <strong><?php echo $_SESSION['message_error']?></strong>
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							  </button>
							</div>
						<?php }
						unset($_SESSION['message_success']);
						unset($_SESSION['message_error']);
						?>
						

						<h3>Genericshop Configuration</h3>	
					</div>
					<input type="hidden" name="store_name" value="<?php echo $shop ?>">					
				  	<div class="col-md-12">
						<label>User id</label>
						<input type="text" name="userid" class="form-control" required value="<?php echo (isset($current_conf->user_id)) ? $current_conf->user_id : "" ?>" />
					</div>
					<div class="col-md-12">
						<label>Password</label>
						<input type="text" name="password" class="form-control" required value="<?php echo (isset($current_conf->password)) ? $current_conf->password : "" ?>" />
					</div>
					<div class="col-md-12">
						<label>Card collection</label>
						<select class="form-control" multiple name="card_collection[]" required id="card_collection">
							<?php while($row =  $cardbrands->fetch_array(MYSQLI_ASSOC)){ ?>
								<option value="<?php echo $row['card_brand'] ?>"><?php echo $row['card_title'] ?></option>
					      	<?php } ?>
						</select>
					</div>
					<div class="col-md-12">
						<label>Entity id</label>
						<input type="text" name="entityid" class="form-control" required value="<?php echo (isset($current_conf->entity_id)) ? $current_conf->entity_id : "" ?>" />
					</div>
					<div class="col-md-12">
						<label>Transaction mode</label>
						<select class="form-control" name="transmode" required>
							<option value="0">Choose transaction mode</option>
							<?php if($current_conf->trans_mod == 'DB'){ ?>
								<option value="DB" selected>Debit</option>
								<option value="PA">PRE-Authorization</option>
							<?php }else if($current_conf->trans_mod == 'PA'){ ?>
								<option value="DB">Debit</option>
								<option value="PA" selected>PRE-Authorization</option>
							<?php }else{ ?>
								<option value="DB">Debit</option>
								<option value="PA">PRE-Authorization</option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-12 mt-3">
						<button class="btn btn-success">Save changes</button>
						<button class="btn btn-danger" type="reset">Reset</button>
					</div>
				</form>
			</div>
		</div>
		<?php include('includes/footer.php'); ?>
		<script type="text/javascript">
			var values='<?php echo ($current_conf->card_collection != "") ? $current_conf->card_collection : "" ?>';
			$.each(values.split(","), function(i,e){
			    $("#card_collection option[value='" + e + "']").prop("selected", true);
			});
		</script>
	</body>
</html>
