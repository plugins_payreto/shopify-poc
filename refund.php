<?php 
	include('con_db/con_db.php');
	$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
	$data = file_get_contents('php://input');
	$verified = verify_webhook($data, $hmac_header);

	if($verified){
		$decodedData = json_decode($data, true);
		$orderId = $decodedData["order_id"];

		error_log("Oder id from shopify ".$orderId."\n", 3, "/var/www/html/shopify-poc/my-errors.log");
		error_log("Oder json shopify ".$data."\n", 3, "/var/www/html/shopify-poc/my-errors.log");

		$lineItems = array();
		$adjustment = array();

		foreach ($decodedData["transactions"] as $item) {
			$lineItems[] = $item["amount"];
		}

		(float) $totalRefund = array_sum($lineItems);

		$finalamount = "";
		$totalAddZero = explode(".", $totalRefund);

		if(count($totalAddZero) == 2){
			if(strlen($totalAddZero[1]) == 1){
				$finalamount = $totalRefund.".0";
			}else{
				$finalamount = $totalRefund;
			}
		}else{
			$finalamount = $totalRefund.".00";
		}

		error_log("Final amount from shopify ".$finalamount."\n", 3, "/var/www/html/shopify-poc/my-errors.log");

		$referenceid = "select * from tbl_transaction where order_id='".$orderId."'";
		$referenceid = $db->query($referenceid);
		$referenceidRes = $referenceid->fetch_object();

		$shopconf = "select * from tbl_store_conf where store_name='".$shop."'";
		$shopconf = $db->query($shopconf);
		$shopconf = $shopconf->fetch_object();

		$refundparams = (object) [
		    'userId' => $shopconf->user_id,
		    'password' => $shopconf->password,
		    'entityId' => $shopconf->entity_id,
		    'amount' => $finalamount,
		    //TO DO YSP
		    'currency' => "EUR",
		    'paymentType' => "RF",
		];

		error_log("refundparams for oppwa ".json_encode($refundparams)."\n", 3, "/var/www/html/shopify-poc/my-errors.log");
		error_log("refundparams for oppwa (referenceid) ".$referenceidRes->reference_id."\n", 3, "/var/www/html/shopify-poc/my-errors.log");
	  	$refund = refundOpp($referenceidRes->reference_id, $refundparams);

	  	error_log("response from oppwa ".$refund."\n", 3, "/var/www/html/shopify-poc/my-errors.log");
	}

?>