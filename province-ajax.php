<?php 
	session_start();
	require 'vendor/autoload.php';
  	use sandeepshetty\shopify_api;
  	include('con_db/con_db.php');

  	$customerinfo = "select * from tbl_shop_customer where cart_token='".$_COOKIE['cart_token']."'";
	$customerinfo = $db->query($customerinfo);
	$customerinfo = $customerinfo->fetch_object();

  	$countryCode = (isset($_GET['country_code'])) ? $_GET['country_code'] : ''; 

	$provinces = shopify_api\getProvince($countryCode, $shop,$usersetting->access_token);
?>
<select id="province" name="province" class="form-control">
	<option value="">-- choose province --</option>
	<?php foreach($provinces as $prov){ 
		if($customerinfo->customer_province == $prov->id){
		setcookie('province_code', $prov->code);
	?>
		<option value="<?php echo $prov->id ?>" selected><?php echo $prov->name ?></option>
	<?php }else{ ?>
		<option value="<?php echo $prov->id ?>"><?php echo $prov->name ?></option>
	<?php } ?>
	<?php } ?>
</select>