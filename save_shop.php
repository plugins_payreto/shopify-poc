<?php
  session_start();

  require 'vendor/autoload.php';
  use sandeepshetty\shopify_api;
  include('con_db/con_db.php');
  if(!empty($_GET['shop']) && !empty($_GET['code'])){
    $shop = $_GET['shop']; //shop name
    //get permanent access token
    $access_token = shopify_api\oauth_access_token(
        $_GET['shop'], $app_settings->api_key, $app_settings->shared_secret, $_GET['code']
    );

    //save the shop details to the database
    $db->query("
       INSERT INTO tbl_usersettings 
       SET access_token = '$access_token',
       store_name = '$shop'
   ");

    //save the signature and shop name to the current session
    $_SESSION['access_token'] = $access_token;
    $_SESSION['shop'] = $shop;
    
    header('Location: https://'.$shop.'/admin/apps/genericshop');
  }
?>