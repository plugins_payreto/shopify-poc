<?php 
	include('con_db/con_db.php');
?>
<!DOCTYPE html>
<html>
	<head>
		 <?php include('includes/header.php'); ?>
	</head>
	<body>
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-md-5 text-center mt-5">
				<h3>Install Genericshop to your shop</h3>
				<label>Enter your shop domain</label>
				<input id="shop" type="text" class="form-control" />
				<button id="install" class="btn btn-success mt-3">Install</button>
			</div>
		</div>
	</div>
	<?php include('includes/footer.php'); ?>
	<script>
		$("#install").click(function(){
			let shop = $("#shop").val();
			if(shop != ""){
				window.location.href="<?php echo $server ?>/shopify-poc?shop="+shop
			}
		});
	</script>
	</body>
</html>