<?php 
	session_start();
	require 'vendor/autoload.php';
  	use sandeepshetty\shopify_api;
  	include('con_db/con_db.php');

  	$customerinfo = "select * from tbl_shop_customer where cart_token='".$_COOKIE['cart_token']."'";
	$customerinfo = $db->query($customerinfo);
	$customerinfo = $customerinfo->fetch_object();

	$checkout_token = (isset($_COOKIE['checkout_token'])) ? $_COOKIE['checkout_token'] : '';

	$ship_rates = shopify_api\getShipRate($shop, $usersetting->access_token, $checkout_token);

	$ship_rates_decoded = json_decode($ship_rates);

	$countShippingrates = count($ship_rates_decoded->shipping_rates);
	setcookie("count_shippingmethod", $countShippingrates);
?>
<table class="table">
 <tbody>
	<?php 
	$i = 1;
	foreach($ship_rates_decoded->shipping_rates as $val){?>
		<tr>
		  <td class="col-md-5">
		  	<span><input id="shipper<?php echo $i ?>" type="checkbox" name="shipper" value="<?php echo $val->handle ?>"/><?php echo $val->title ?></span>
		  </td>
		  <td class="col-md-7 text-right"><?php echo $val->price ?></td>
		</tr>
		<script type="text/javascript">
			$("#shipper<?php echo $i ?>").click(function(){
				let shipper = $("#shipper<?php echo $i ?>").is(':checked');
				if(shipper){
					setCookie('total_tax', '<?php echo $val->checkout->total_tax ?>');
					setCookie('total_price', '<?php echo $val->checkout->total_price ?>');
					setCookie('subtotal_price', '<?php echo $val->checkout->subtotal_price ?>');
					setCookie('shipping_price', '<?php echo $val->price ?>');
					setCookie('shipping_title', '<?php echo $val->title ?>');
					setCookie('shipping_handle', '<?php echo $val->handle ?>');
					setCookie('shipping_for_patch', '<?php echo json_encode($val) ?>')
					$.ajax({
						url: "cart-ajax.php?shop=<?php echo $shop ?>",
						method: "GET"
					}).done(function(data){
						$("#cart").empty();
						$("#cart").append(data);
					})
				}
			});
		</script>
	<?php $i++;} ?>
  </tbody>
</table>