<?php 
	session_start();
  	include('con_db/con_db.php');

  	
  	$shopconf = "select * from tbl_store_conf where store_name='".$shop."'";
	$shopconf = $db->query($shopconf);
	$shopconf = $shopconf->fetch_object();

	$cardcollection = str_replace(",", " ", $shopconf->card_collection);

	(float) $amount = $_COOKIE['total_price'];
	$finalamount = "";
	$totalAddZero = explode(".", $amount);
	if(count($totalAddZero) == 2){
		if(strlen($totalAddZero[1]) == 1){
			$finalamount = $amount."0";
		}else{
			$finalamount = $amount;
		}
	}

	$currency = (isset($_COOKIE['currency'])) ? $_COOKIE['currency'] : "";

	$checkoutparams = (object) [
	    'userId' => $shopconf->user_id,
	    'password' => $shopconf->password,
	    'entityId' => $shopconf->entity_id,
	    'amount' => $finalamount,
	    'currency' => $currency,
	    'paymentType' => $shopconf->trans_mod,
	];

  	$createCheckout = createCheckoutOpp($oppwaserver, $checkoutparams);
  	$createCheckout = json_decode($createCheckout);

  	$redirect_url = $server."/shopify-poc/create-order-transaction.php?shop=".$shop."&oppwacheckout=".$createCheckout->id;
?>
<script src="https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=<?php echo $createCheckout->id ?>"></script>
<form action="<?php echo $redirect_url ?>" class="paymentWidgets" data-brands="<?php echo $cardcollection ?>"></form>