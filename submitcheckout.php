<?php 
	session_start();
	require 'vendor/autoload.php';
  	use sandeepshetty\shopify_api;
  	include('con_db/con_db.php');

  	$customerinfo = "select * from tbl_shop_customer where cart_token='".$_COOKIE['cart_token']."'";
	$customerinfo = $db->query($customerinfo);
	$customerinfo = $customerinfo->fetch_object();

	$country_code = (isset($_COOKIE['country_code'])) ? $_COOKIE['country_code'] : '';
	$province_code = (isset($_COOKIE['province_code'])) ? $_COOKIE['province_code'] : '';
	$line_items = json_decode($_COOKIE['cart_items']);
	$items = array();
	foreach($line_items as $item){
		$items[] = array(
			"variant_id" => $item->variant_id,
			"quantity" => $item->quantity
		);
	}

	$shipping_address = array(
		"first_name" => $customerinfo->customer_firstname,
		"last_name" => $customerinfo->customer_lastname,
		"address1" => $customerinfo->customer_address,
		"city" => $customerinfo->customer_city,
		"province_code" => $province_code,
		"country_code" => $country_code,
		"zip" => $customerinfo->customer_postal_code
	);
	

	$checkout = array();
	$checkout['checkout'] = array(
		"email" => $customerinfo->customer_contact,
		"line_items" => $items,
		"shipping_address" => $shipping_address
	);

	$jsoncheckout = json_encode($checkout);

	$result = shopify_api\createCheckout($shop, $usersetting->access_token, $jsoncheckout);
	echo $result;
?>