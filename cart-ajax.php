<?php 
	session_start();
	include('con_db/con_db.php');

	$cart_from_cookie = (isset($_COOKIE['cart_items'])) ? $_COOKIE['cart_items'] : '';
	$cart_subtotal = (isset($_COOKIE['cart_total_price'])) ? $_COOKIE['cart_total_price'] : '';
	$cart_subtotal = formatTwoZero($cart_subtotal);
	$product = json_decode($cart_from_cookie);
	$taxes = (isset($_COOKIE['total_tax'])) ? $_COOKIE['total_tax'] : 0;
	$shipping = (isset($_COOKIE['shipping_price'])) ? $_COOKIE['shipping_price'] : 0;
	$total = $cart_subtotal + $taxes + $shipping;
	$totalAddZero = explode(".", $total);
	$finaltotal = "";
	if(count($totalAddZero) == 2){
		if(strlen($totalAddZero[1]) == 1){
			$finaltotal = $total."0";
		}else{
			$finaltotal = $total;
		}
	}else{
		$finaltotal = $total.".00";
	}
?>
<div class="col-md-12"  style="border-bottom-style: solid;border-bottom-width: 1px">
	<?php foreach($product as $prod){
		$prodPrice = formatTwoZero($prod->price);
	?>
		<div class="row">
			<div class="col-md-6 pl-0">
				<p><?php echo $prod->title ?> <span class="badge badge-warning"><?php echo $prod->quantity ?></span></p>
			</div>
			<div class="col-md-6 text-right pr-0">
				<span><?php echo $prodPrice; ?></span>
			</div>
		</div>
	<?php } ?>
</div>
<div class="col-md-12"  style="border-bottom-style: solid;border-bottom-width: 1px">
	<div class="row">
		<div class="col-md-6 pl-0">
			<p>Sub total</p>
		</div>
		<div class="col-md-6 text-right pr-0">
			<span><?php echo $cart_subtotal ?></span>
		</div>
		<div class="col-md-6 pl-0">
			<p>Shipping</p>
		</div>
		<div class="col-md-6 text-right pr-0">
			<span><?php echo $shipping ?></span>
		</div>
		<div class="col-md-6 pl-0">
			<p>Taxes</p>
		</div>
		<div class="col-md-6 text-right pr-0">
			<span><?php echo $taxes ?></span>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
		<div class="col-md-6 pl-0">
			<p>Total</p>
		</div>
		<div class="col-md-6 text-right pr-0">
			<span><?php echo $finaltotal ?></span>
		</div>
	</div>
</div>