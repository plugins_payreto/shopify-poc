<?php	
	$db = new Mysqli("127.0.0.1", "root", "admin123", "shopifypoc");

	if($db->connect_errno){
	  die('Connect Error: ' . $db->connect_errno);
	}

	$select_settings = $db->query("SELECT * FROM tbl_appsettings WHERE id = 1");
	$app_settings = $select_settings->fetch_object();
	$server = "https://31fe3c33.ngrok.io";
	$oppwaserver = "https://test.oppwa.com/v1/checkouts";

	if(isset($_GET["shop"])){
  		$shop = $_GET["shop"];
  		$sql_getCurrentShopData = $db->query("SELECT access_token from tbl_usersettings WHERE store_name='".$shop."'");
	  	if($sql_getCurrentShopData->num_rows > 0){
	  		$usersetting = $sql_getCurrentShopData->fetch_object();
	  		if($usersetting->access_token == "" && $usersetting->store_name == ""){
	  			echo "NOT AUTHORIZE";die();
	  		}
	  		
	  	}
  	}else{
  		echo "NOT AUTHORIZE";die();
  	}

  	$current_conf = $db->query("SELECT * FROM tbl_store_conf WHERE store_name='".$shop."'");
  	$current_conf = $current_conf->fetch_object();

	function insertInto($table, $data, $db){
		$fields = $values = array();

		foreach( array_keys($data) as $key ) {
	        if( !in_array($key, $data) ) {
	            $fields[] = "`$key`";
	            $values[] = "'" . mysqli_real_escape_string($db, $data[$key]) . "'";
	        }
	    }

	    $insert = "INSERT INTO $table (".implode(",",$fields).") VALUES (".implode(",",$values).")";

	    if($db->query($insert)){
			return true;
		}else{
			return false;
		}
	}

	function updateTable($table, $data, $db){
		$cart_token = $_COOKIE['cart_token'];
		if (count($data) > 0) {
            foreach ($data as $key => $value) {

                $value = mysqli_real_escape_string($db, $value); // this is dedicated to @Jon
                $value = "'$value'";
                $updates[] = "$key = $value";
            }
        }

        $implodeArray = implode(', ', $updates);
        $update = "UPDATE $table SET $implodeArray WHERE cart_token='".$cart_token."'";

	    if($db->query($update)){
			return true;
		}else{
			return false;
		}
	}

	function createCheckoutOpp($oppwaserver, $params){
		$url = $oppwaserver;
		$data = "authentication.userId=".$params->userId .
			"&authentication.password=".$params->password .
			"&authentication.entityId=".$params->entityId .
			"&amount=".$params->amount .
			"&currency=".$params->currency .
			"&paymentType=".$params->paymentType;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$responseData = curl_exec($ch);
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		return $responseData;
	}

	function checkstatusOpp($oppwaId, $params){
		$url = "https://test.oppwa.com/v1/checkouts/".$oppwaId."/payment";
		$url .= "?authentication.userId=".$params->userId;
		$url .= "&authentication.password=".$params->password;
		$url .= "&authentication.entityId=".$params->entityId;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$responseData = curl_exec($ch);
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		return $responseData;
	}

	function refundOpp($oppwaId, $params) {
		$url = "https://test.oppwa.com/v1/payments/".$oppwaId;
		$data = "authentication.userId=".$params->userId .
			"&authentication.entityId=".$params->entityId .
			"&authentication.password=".$params->password .
			"&amount=".$params->amount .
			"&currency=".$params->currency .
			"&paymentType=".$params->paymentType;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$responseData = curl_exec($ch);
		if(curl_errno($ch)) {
			return curl_error($ch);
		}
		curl_close($ch);
		return $responseData;
	}

	function formatTwoZero($numbers){
		$length=strlen($numbers);
		$temp1=substr($numbers,0,$length-2);
		$temp2=substr($numbers,$length-2,$length);
		$finalFormat = $temp1.".".$temp2; 

		return $finalFormat;
	}

	define("shared_secret", "6838f35a801e7abd282d6cb2ce0ea712435f674e06bd20982cf88940dc1e939d");
	function verify_webhook($data, $hmac_header)
	{
	  $calculated_hmac = base64_encode(hash_hmac('sha256', $data, shared_secret, true));
	  return hash_equals($hmac_header, $calculated_hmac);
	}
?>
