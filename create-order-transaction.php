<?php 
	session_start();
	require 'vendor/autoload.php';
  	use sandeepshetty\shopify_api;
  	include('con_db/con_db.php');

  	$error = 0;
  	$checkoutid = $_GET['oppwacheckout'];

  	$shopconf = "select * from tbl_store_conf where store_name='".$shop."'";
	$shopconf = $db->query($shopconf);
	$shopconf = $shopconf->fetch_object();

	$customerinfo = "select * from tbl_shop_customer where cart_token='".$_COOKIE['cart_token']."'";
	$customerinfo = $db->query($customerinfo);
	$customerinfo = $customerinfo->fetch_object();

	$checkoutparams = (object) [
	    'userId' => $shopconf->user_id,
	    'password' => $shopconf->password,
	    'entityId' => $shopconf->entity_id
	];

	$status = checkstatusOpp($checkoutid, $checkoutparams);
	$status = json_decode($status);

	if(preg_match('/(000\.000\.|000\.100\.1|000\.[36])/', $status->result->code)){
		$getDataCheckout = shopify_api\getCheckout($_COOKIE["checkout_token"], $shop, $usersetting->access_token);

		$shippingline = array();
		$shippingline[] = array(
			"code" => $_COOKIE["shipping_handle"],
			"price" => $_COOKIE["shipping_price"],
			"source" => "shopipify",
			"title" => $_COOKIE["shipping_title"],
			"tax_line" => array(
				"title" => "",
				"price" => "",
				"rate" => "",
			),
			"carrier_identifier" => "",
			"requested_fulfillment_service_id" => ""
		);

		$shippingrate = array(
			"id" => $_COOKIE["shipping_handle"],
			"price" => $_COOKIE["shipping_price"],
			"title" => $_COOKIE["shipping_title"]
		);

		(float) $amount = $_COOKIE['total_price'] + $_COOKIE['total_tax'];
		$transactions = array();
		$transactions[] = array(
			"amount" => $amount,
			"kind" => "capture",
			"status" => "success",
			"currency" => $_COOKIE["currency"],
			"gateway" => "Genericshop"
		);

		$data = array();
		$data["order"] = array(
			"line_items" => $getDataCheckout->checkout->line_items,
			"tax_lines" => $getDataCheckout->checkout->tax_lines,
			"shipping_lines" => $shippingline,
			"shipping_rate" => $shippingrate,
			"shipping_address" => $getDataCheckout->checkout->shipping_address,
			"credit_card" => null,
			"billing_address" => $getDataCheckout->checkout->shipping_address,
			"transactions" => $transactions,
			"email" => $customerinfo->customer_contact
		);
		$postData = json_encode($data);
		$success = shopify_api\createOrder($shop, $usersetting->access_token, $postData);
		$response = json_decode($success);
		if($response->order != null){
			if (isset($_SERVER['HTTP_COOKIE'])) {
			    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			    foreach($cookies as $cookie) {
			        $parts = explode('=', $cookie);
			        $name = trim($parts[0]);
			        setcookie($name, '', time()-1000);
			        setcookie($name, '', time()-1000, '/');
			    }
			}

			$datas = array(
				"order_id" => $response->order->id,
				"reference_id" => $status->id
			);
			$insertToLog = insertInto("tbl_transaction", $datas, $db);
			if($insertToLog){
				header("Location: ".$response->order->order_status_url);
			}
		}else{
			$error++;
		}

	}else{
		$error++;
	}

?>
<?php if($error > 0){ ?>
	<!DOCTYPE html>
	<html>
		<head>
			 <?php include('includes/header.php'); ?>
		</head>
		<body>
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<?php echo $status->result->description ?>
					</div>
				</div>
			</div>
			<?php include('includes/footer.php'); ?>
		</body>
	</html>
<?php } ?>