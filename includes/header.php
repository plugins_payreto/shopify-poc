<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Shopify POC</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<style>
	.breadcrumb-item{cursor:pointer}
	.loader {
	  border: 8px solid #f3f3f3;
	  border-radius: 50%;
	  border-top: 8px solid #000;
	  width: 50px;
	  height: 50px;
	  -webkit-animation: spin 0.6s linear infinite; /* Safari */
	  animation: spin 0.6s linear infinite;
	  margin: auto;
	}

	/* Safari */
	@-webkit-keyframes spin {
	  0% { -webkit-transform: rotate(0deg); }
	  100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
</style>