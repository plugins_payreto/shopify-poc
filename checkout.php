<?php 
	session_start();
	require 'vendor/autoload.php';
  	use sandeepshetty\shopify_api;
  	include('con_db/con_db.php');

  	$shopproperties = shopify_api\getShopProperties($shop, $usersetting->access_token);
  	$shopproperties = json_decode($shopproperties);
?>
<!DOCTYPE html>
<html>
	<head>
		 <?php include('includes/header.php'); ?>
	</head>
	<body>
		 <div class="container mt-5">
           <div class="row">
           	 <div class="col-md-12 pr-0">
				<nav aria-label="breadcrumb">
				  <ol class="breadcrumb">
				    <li id="ci" class="breadcrumb-item" onclick="callAjax('checkout-customerinfo.php')">Customer information</li>
				    <li id="sm" class="breadcrumb-item" onclick="callAjax('checkout-shipping.php')">Shipping method</li>
				    <li id="pm" class="breadcrumb-item" onclick="callAjax('checkout-payment.php')">Payment method</li>
				    <li id="pw" class="breadcrumb-item" onclick="callAjax('checkout-paymentwidget.php')">Payment widget</li>
				  </ol>
				</nav>
			 </div>
			</div>

			<div class="row">
				<div class="col-md-7" id="ajaxstep">
					<div class="loader"></div>
				</div>
				<div class="col-md-5" style="background-color:#f2f2f2">
					<h4>Cart item(s)</h4>
					<div id="cart">
						<div class="loader"></div>
					</div>
				</div>
			</div>
		</div>

		<?php include('includes/footer.php'); ?>
		<script type="text/javascript">
			let menus = JSON.parse(window.localStorage.getItem('checkoutflow'));
			
			if(!menus){
				menus = {"customerinfo":true, "shipping":false, "payment":false, "paymentwidget":false};
				window.localStorage.setItem('checkoutflow', JSON.stringify(menus));
			}

			function ajaxSend(urls){
				$("#ajaxstep" ).empty();
				$("#ajaxstep").append('<div class="loader"></div>');
				$.ajax({
				  url: urls+'?shop=<?php echo $shop ?>',
				  cache: false
				})
				  .done(function( html ) {
				  	if(urls != "cart-ajax.php"){
				  		$( "#ajaxstep" ).empty();
				    	$( "#ajaxstep" ).append( html );
				  	}else{
				  		$( "#cart" ).empty();
				    	$( "#cart" ).append( html );
				  	}
				  	
				  });
			}

			function setActive(urls){
				if(urls == "checkout-customerinfo.php"){
					$("#ci").addClass('active');
					$("#sm").removeClass('active');
					$("#pm").removeClass('active');
					$("#pw").removeClass('active');
				}else if(urls == "checkout-shipping.php"){
					$("#sm").addClass('active');
					$("#ci").removeClass('active');
					$("#pm").removeClass('active');
					$("#pw").removeClass('active');
				}else if(urls == "checkout-payment.php"){
					$("#pm").addClass('active');
					$("#sm").removeClass('active');
					$("#ci").removeClass('active');
					$("#pw").removeClass('active');
				}else if(urls == "checkout-paymentwidget.php"){
					$("#pw").addClass('active');
					$("#pm").removeClass('active');
					$("#sm").removeClass('active');
					$("#ci").removeClass('active');
				}
			}

			function callAjax(urls){

				let menus = JSON.parse(window.localStorage.getItem('checkoutflow'));

				if(urls == "checkout-customerinfo.php"){
					 if(menus.customerinfo){
					 	ajaxSend(urls);
						setActive(urls);
					 }
				}

				if(urls == "checkout-shipping.php"){
					 if(menus.shipping){
					 	ajaxSend(urls);
						setActive(urls);
					 }
				}

				if(urls == "checkout-payment.php"){
					 if(menus.payment){
					 	ajaxSend(urls);
						setActive(urls);
					 }
				}

				if(urls == "checkout-paymentwidget.php"){
					 if(menus.paymentwidget){
					 	ajaxSend(urls);
						setActive(urls);
					 }
				}

				if(urls == "cart-ajax.php"){
					ajaxSend(urls);
				}
			}

			function nextStep(nextUrl){
				let res = validateField(nextUrl);
				let error = res.error;
				let data = res.data;
			 	let menus = "";

			 	if(error == 0){
					if(nextUrl == "checkout-shipping.php"){
						submitData(data, "submitcustomer.php?shop=<?php echo $shop ?>");
						submitCheckout();
						ajaxSend(nextUrl);
						setActive(nextUrl);
						menus = window.localStorage.getItem('checkoutflow');
						menus = JSON.parse(menus);	
						menus.shipping = true;

					}else{
						ajaxSend(nextUrl);
						setActive(nextUrl);
						menus = window.localStorage.getItem('checkoutflow');
						menus = JSON.parse(menus);	

						if(nextUrl == "checkout-payment.php"){
							menus.payment = true;
						}
					}
			 	}else{
			 		alert('Please complete the form');
			 		menus = window.localStorage.getItem('checkoutflow');
					menus = JSON.parse(menus);
					if(nextUrl == "checkout-shipping.php"){
						menus.shipping = false;
					}

					if(nextUrl == "checkout-payment.php"){
						menus.payment = false;
					}
			 	}

			 	window.localStorage.setItem('checkoutflow', JSON.stringify(menus));
			}

			function validateField(form){
				let error = 1;
				let data = {};
				let res = {"error": error, "data":data};
				if(form == "checkout-shipping.php"){
					let contact = $("#contact").val();
					let firstname = $("#firstname").val();
				 	let lastname = $("#lastname").val();
				 	let address = $("#address").val();
				 	let city = $("#city").val();
				 	let country = $("#country").val();
				 	let province = $("#province").val();
				 	let code = $("#code").val();
				 	let shop = "<?php echo (isset($_GET['shop'])) ? $_GET['shop'] : '' ?>";
				 	let cart_token = getCookie('cart_token');

				 	if(firstname != ""
				 	&& contact != ""
				 	&& lastname != ""
				 	&& address != ""
				 	&& city != ""
				 	&& country != ""
				 	//&& province != ""
				 	&& code != ""
				 	&& shop != ""){
				 		data.customer_contact = contact;
				 		data.customer_firstname = firstname;
				 		data.customer_lastname = lastname;
				 		data.customer_address = address;
				 		data.customer_city = city;
				 		data.customer_country = country;
				 		data.customer_province = province;
				 		data.customer_postal_code = code;
				 		data.shop = shop;
				 		data.cart_token = cart_token;
				 		error = 0;
				 	}
				}

				if(form == "checkout-payment.php"){
				 	let shipment_count = getCookie('count_shippingmethod');
				 	let i = 1;
				 	for(i;i<=shipment_count;i++){
				 		if($("#shipper"+i).is(':checked')){
				 			shipperVal = $("#shipper"+i).val();
							error = 0;
				 		}
				 	}
				}

				if(form == "checkout-paymentwidget.php"){
			 		if($("#payment-method").is(':checked')){
						error = 0;
			 		}
				}

				res.error = error;
				res.data = data;
				return res;
			}

			function submitData(datas, link){
				let datasub = JSON.stringify(datas);
				$.post(link, {"values" : datasub})
				  .done(function( data ) {
				    console.log( "Data Loaded: " + data );
				  })
				  .fail(function(error){
				  	console.log("error", error);
				  });
			}

			function submitCheckout(){
				$.ajax({
				  url: 'submitcheckout.php?shop=<?php echo $shop ?>',
				  method: 'GET'
				}).done(function(data){
					let jsonCheckout = JSON.parse(data);
					if(jsonCheckout != ""){
						setCookie('checkout_token', jsonCheckout.checkout.token);
						console.log("checkout_token", jsonCheckout.checkout.token);
					}else{
						console.log("retry");
					}
					
				});
			}

			function setCookie(cname, cvalue, exdays) {
			    var d = new Date();
			    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
			    var expires = "expires="+d.toUTCString();
			    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
			}

			function getCookie(cname) {
			    var name = cname + "=";
			    var ca = document.cookie.split(';');
			    for(var i = 0; i < ca.length; i++) {
			        var c = ca[i];
			        while (c.charAt(0) == ' ') {
			            c = c.substring(1);
			        }
			        if (c.indexOf(name) == 0) {
			            return c.substring(name.length, c.length);
			        }
			    }
			    return "";
			}

			$(document).ready(function(){
				let cart = "<?php echo 'https://'.$shop.'/cart.json' ?>";
				let country = "<?php echo 'https://'.$shop.'/admin/countries.json' ?>";
				$.ajax({
				    url: cart,
				    method: "GET",
				    dataType: "jsonp",

				    success: function( response ) {
				    	setCookie('cart_token', response.token);
				    	setCookie('cart_total_price', response.total_price);
				    	setCookie('cart_item_count', response.item_count);
				    	setCookie('cart_items', JSON.stringify(response.items));
				    	callAjax("cart-ajax.php");
				    	callAjax("checkout-customerinfo.php");
				        console.log( response );
				    }
				});
				setCookie("currency", "<?php echo $shopproperties->shop->currency ?>");
			});
		</script>
	</body>
</html>