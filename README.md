# override shopify default checkout
#### Add this script to cart-template.liquid
```html
<input id="checkoutgen" type="button" name="checkout"class="btn btn--small-wide cart__submit cart__submit-control" value="{{ 'cart.general.checkout' | t }}">
{{ '//ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js' | script_tag }}
<script type="text/javascript">
  $("#checkoutgen").click(function(){
  	window.location.href = "https://b88a5fab.ngrok.io/shopify-poc/checkout.php?shop=yosephtest.myshopify.com";
  });
</script>
```

#### disable email customer for abondoned checkout
Go to settings -> checkout -> uncheck "Automatically send abandoned checkout emails" 

#### clear cart manualy in custom order confirmation page
```html
<script>
   document.cookie = "cart_sig" + '=;expires=Thu, 2 Aug 1970 00:00:01 UTC;path=/';
   document.cookie = "cart_ts" + '=;expires=Thu, 2 Aug 1970 00:00:01 UTC;path=/';
   document.cookie = "cart" + '=;expires=Thu, 2 Aug 1970 00:00:01 UTC;path=/';
	window.onload = function() {
	    if(!window.location.hash) {
		window.location = window.location + '#loaded';
		window.location.reload();
	    }
	}
</script>
```
#### Create webhook to trigger refund
Go settings -> notifications -> webhook -> create webhook
