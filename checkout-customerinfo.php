<?php
    session_start();
    require 'vendor/autoload.php';
      use sandeepshetty\shopify_api;
      include('con_db/con_db.php');

    $customerinfo = "select * from tbl_shop_customer where cart_token='".$_COOKIE['cart_token']."'";
    $customerinfo = $db->query($customerinfo);
    $customerinfo = $customerinfo->fetch_object();
    $countries = shopify_api\getCountry($shop, $usersetting->access_token);
?>
<h4>Customer information</h4>
<form class="col-md-12 pl-0">
  <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
      <label class="control-label" for="contact">Email or phone number :</label>
  </div>
  <div class="form-group col-xs-12 col-md-12 pb-0 mb-5 pl-0">
    <input type="text" id="contact" name="contact" class="form-control" value="<?php echo (isset($customerinfo->customer_contact)) ? $customerinfo->customer_contact:'' ?>" />
  </div>
</form>
<h4>Shipping address information</h4>
<form class="col-md-12 pl-0">
    <div class="row">
      <div class="col-md-6">
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
              <label class="control-label" for="firstname">Firstname :</label>
          </div>
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
            <input type="text" id="firstname" name="firstname" class="form-control" value="<?php echo (isset($customerinfo->customer_firstname)) ? $customerinfo->customer_firstname:'' ?>" />
          </div>
      </div>
      <div class="col-md-6">
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
              <label class="control-label" for="lastname">Lastname :</label>
          </div>
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
            <input type="text" id="lastname" name="lastname" class="form-control" value="<?php echo (isset($customerinfo->customer_lastname)) ? $customerinfo->customer_lastname:'' ?>"/>
          </div>
      </div>
      <div class="col-md-12">
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
              <label class="control-label" for="address">Address :</label>
          </div>
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
            <input type="text" id="address" name="address" class="form-control" value="<?php echo (isset($customerinfo->customer_address)) ? $customerinfo->customer_address:'' ?>"/>
          </div>
      </div>
      <div class="col-md-12">
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
              <label class="control-label" for="city">City :</label>
          </div>
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
            <input type="text" id="city" name="city" class="form-control" value="<?php echo (isset($customerinfo->customer_city)) ? $customerinfo->customer_city:'' ?>"/>
          </div>
      </div>
      <div class="col-md-4">
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
              <label class="control-label" for="country">Country :</label>
          </div>
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-5 pl-0">
              <select id="country" name="country" class="form-control">
                  <option value="">-- choose country --</option>
                  <?php foreach($countries->countries as $country){ ?>
                       <?php if($country->code != "*"){ ?>
                           <?php if($country->id == $customerinfo->customer_country){
                              setcookie('country_code', $country->code);
                            ?>
                            <option value="<?php echo $country->id ?>" selected><?php echo $country->name ?></option>
                        <?php }else{ ?>
                            <option value="<?php echo $country->id ?>"><?php echo $country->name ?></option>
                        <?php } ?>
                    <?php } ?>
                  <?php } ?>
              </select>
          </div>
      </div>
      <div class="col-md-4">
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
              <label class="control-label" for="province">Province :</label>
          </div>
          <div id="province-select" class="form-group col-xs-12 col-md-12 pb-0 mb-5 pl-0">
              <select id="province" name="province" class="form-control">
                  <option value="">-- choose province --</option>
              </select>
          </div>
      </div>
      <div class="col-md-4">
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-0 pl-0">
              <label class="control-label" for="code">Postal code :</label>
          </div>
          <div class="form-group col-xs-12 col-md-12 pb-0 mb-5 pl-0">
            <input type="text" id="code" name="code" class="form-control" value="<?php echo (isset($customerinfo->customer_postal_code)) ? $customerinfo->customer_postal_code:'' ?>" />
          </div>
      </div>
    </div>
</form>
<div class="col-md-12 pl-0">
    <span class="float-left"><a href="https://<?php echo $shop ?>/cart"> Back to shop </a></span>
    <button class="btn btn-info float-right" onclick="nextStep('checkout-shipping.php')">Continue to shipping method</button>
</div>
<script type="text/javascript">
    $("#country").change(function(){
        let country_code = $("#country").val();
        $.ajax({
            url: 'province-ajax.php?shop=<?php echo $shop ?>&country_code='+country_code,
            method: "GET",
            success : function(response){
              console.log("res", response);
                $("#province-select").empty();
                $("#province-select").append(response);
            }

        })
    });

    <?php if(isset($customerinfo->customer_country) || isset($customerinfo->customer_country)){ ?>
        $(document).ready(function(){
            let country_code = $("#country").val();
            $.ajax({
                url: 'province-ajax.php?shop=<?php echo $shop ?>&country_code='+country_code,
                method: "GET",
                success : function(response){
                    $("#province-select").empty();
                    $("#province-select").append(response);
                }

            })
        });
    <?php } ?>
</script>

