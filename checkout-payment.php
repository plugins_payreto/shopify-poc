 <?php 
 	session_start();
 	require 'vendor/autoload.php';
  	use sandeepshetty\shopify_api;
  	include('con_db/con_db.php');
	$customerinfo = "select * from tbl_shop_customer where cart_token='".$_COOKIE['cart_token']."'";
	$customerinfo = $db->query($customerinfo);
	$customerinfo = $customerinfo->fetch_object();

	$shopconf = "select card_collection from tbl_store_conf where store_name='".$shop."'";
	$shopconf = $db->query($shopconf);
	$shopconf = $shopconf->fetch_object();

	$selectedCard = explode(",",$shopconf->card_collection);
	$selectedCard =  implode("','",$selectedCard);

	$cardbrands = "select * from tbl_card_brands where card_brand in('".$selectedCard."')";
	$cardbrands = $db->query($cardbrands);
 ?>
 <style>
	 table{
	 	 border-collapse: collapse;
	 	 border: 1px solid #dee2e6;
	 }
 </style>
 <div class="col-md-12">
   <div class="row">
	   	<div class="table-responsive">
	   		<table class="table">
			  <tbody>
			    <tr>
			      <td class="col-md-1">Contact</td>
			      <td class="col-md-8"><?php echo (isset($customerinfo->customer_contact)) ? $customerinfo->customer_contact:'' ?></td>
			    </tr>
			    <tr>
			      <td>Ship to</td>
			      <td><?php echo (isset($customerinfo->customer_address)) ? $customerinfo->customer_address:'' ?></td>
			    </tr>
			    <tr>
			      <td>Method</td>
			      <td><?php echo (isset($_COOKIE['shipping_title'])) ? $_COOKIE['shipping_title']:'' ?></td>
			    </tr>
			  </tbody>
			</table>
		</div>
		<h4>Payment method</h4>
		<div class="table-responsive">
	   		<table class="table">
			  <tbody>
			    <tr>
			      <td class="col-md-5">
			      	<span><input id="payment-method" name="payment-method" type="checkbox" /> 
			      	<?php while($row =  $cardbrands->fetch_array(MYSQLI_ASSOC)){ ?>
						<img src="<?php echo $row['card_image'] ?>" alt="<?php echo $row['card_title'] ?>" width="50px"/>
			      	<?php } ?>
			      	</span>
			      </td>
			    </tr>
			  </tbody>
			</table>
		</div>
   </div>
</div>
<div class="col-md-12 pl-0">
	<span class="float-left" onclick="callAjax('checkout-shipping.php')" style="color:#007bff;cursor: pointer;">Return to shipping method</span>
	<button class="btn btn-info float-right" onclick="nextStep('checkout-paymentwidget.php')">Complete order</button>
</div>
